<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMcDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mc_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('valid')->default(false);
            $table->datetime('time')->useCurrent();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->double('current_liters');
            $table->double('consumption')->default(0);
            $table->double('aquifier_level');
            $table->string('uuid')->nullable();
            $table->string('microcontroller_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mc_datas');
    }
}
