<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mc_datas', function (Blueprint $table) {
            $table->foreign('microcontroller_id')->references('id')->on('microcontrollers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mc_datas', function (Blueprint $table) {
            $table->dropForeign(['microcontroller_id']);
        });
    }
}
